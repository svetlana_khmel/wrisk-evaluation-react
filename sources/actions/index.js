import axios from 'axios';
import * as actionTypes from './actionTypes';
import { getTime, getTimezone } from '../Utils';
import { urlIp, ipstackBaseUrl, climatCellBase, iostackAccessKey, climatCellSectretKey } from '../Api/settings';

const setTimeline = (data, locationData) => ({ type: actionTypes.SET_TIMELINE, payload: { data, locationData } });
const setDetails = index => ({ type: actionTypes.SET_DETAILS, payload: index });

const getIp = () => dispatch => {
  axios
    .get(urlIp)
    .then(data => {
      console.log(data);
      dispatch(getCity(data.data));
    })
    .catch(error => console.log(error));
};

const getCity = ip => dispatch => {
  axios
    .get(`${ipstackBaseUrl}/${ip}?access_key=${iostackAccessKey}`)
    .then(locationData => dispatch(getTimeline(locationData)))
    .catch(err => console.log('Get city err, ', err));
};

const getTimeline = locationData => displatch => {
  const { latitude, longitude } = locationData.data;
  const startTime = getTime();
  const endTime = getTime('future');
  const timezone = getTimezone();

  const url = `${climatCellBase}/timelines?location=${latitude},${longitude}&startTime=${startTime}&endTime=${endTime}&fields=temperature&fields=weatherCode&fields=visibility&fields=precipitationType&fields=precipitationProbability&fields=windSpeed&timesteps=1h&units=metric&timezone=${timezone}&apikey=${climatCellSectretKey}`;

  axios.get(url)
    .then(data => displatch(setTimeline(data.data, locationData)))
    .catch(err => console.error(err));
};

export { getIp, setDetails };
