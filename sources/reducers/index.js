import * as actionTypes from '../actions/actionTypes';

const initialState = {
  details: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_TIMELINE:
      const { data, locationData } = action.payload;
      return { ...state, timelines: data.data.timelines, locationData };
    case actionTypes.SET_DETAILS:
      return { ...state, details: action.payload };

    default:
      return state;
  }
};

export default reducer;
