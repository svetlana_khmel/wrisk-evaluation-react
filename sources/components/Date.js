import React from 'react';
import { getDayTomorrow, getCurrentMonth } from '../Utils';

const Date = () => <>{`${getDayTomorrow()} ${getCurrentMonth()}`}</>;

export default Date;
