import React, { memo } from 'react';

const City = ({ locationData }) => {
  return <div className="city">{locationData.data.city}</div>;
};

export default memo(City);
