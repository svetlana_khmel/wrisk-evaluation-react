import React, { memo } from 'react';
import PreviewCard from './PreviewCard';

import '../scss/timeline.scss';

const Timeline = ({ timelines }) => {
  const renderCards = () => {
    return timelines[0].intervals.map((el, index) => {
      return <PreviewCard key={el.startTime} startTime={el.startTime} values={el.values} index={index} />;
    });
  };

  return <div className="timeline">{renderCards()}</div>;
};

export default memo(Timeline);
