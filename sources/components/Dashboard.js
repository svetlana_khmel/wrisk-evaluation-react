import React from 'react';
import Timeline from './Timeline';

const Dashboard = ({ timelines }) => (
  <div className="overflow">
    <Timeline timelines={timelines} />
  </div>
);

export default Dashboard;
