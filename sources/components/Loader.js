import React, { memo } from 'react';

import '../scss/loader.scss';

const Loader = () => <div className="lds-dual-ring">...</div>;

export default memo(Loader);
