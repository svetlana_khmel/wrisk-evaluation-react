import React, { Suspense } from 'react';
import Loader from '../Loader';

const withSuspense = Component => ({ ...props }) => (
  <Suspense fallback={<Loader />}>
    <Component {...props} />
  </Suspense>
);

export default withSuspense;
