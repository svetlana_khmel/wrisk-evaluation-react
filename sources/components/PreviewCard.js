import React, { useCallback, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setDetails } from '../actions';
import { ThemeContext } from './ThemeProvider';
import { timeToHours, mapCodeToImg } from '../Utils';

const PreviewCard = ({ startTime, values, index }) => {
  const details = useSelector(state => state.details);
  const { theme } = useContext(ThemeContext);
  const { weatherCode, temperature } = values;
  const hours = timeToHours(startTime);
  const imgUrl = mapCodeToImg(weatherCode);

  const dispatch = useDispatch();

  const showDetails = useCallback(() => {
    dispatch(setDetails(index));
  }, []);

  return (
    <div className={`card ${details === index ? 'active' : ''}`} onClick={showDetails}>
      <div className="time">{hours}</div>
      <div style={{ backgroundImage: `url(../public/assets/${theme}/${imgUrl}.svg)` }} className="img"></div>
      <div className="temperature">{temperature}</div>
    </div>
  );
};

export default PreviewCard;
