import React, { memo } from 'react';

const Button = ({ value, name, event }) => (
  <button className={name} onClick={event}>
    {value}
  </button>
);

export default memo(Button);
