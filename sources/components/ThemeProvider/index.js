import React, { useState } from 'react';

export const ThemeContext = React.createContext({});

export const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState('color');

  const toggleTheme = () => {
    setTheme(theme === 'color' ? 'black' : 'color');
  };

  return <ThemeContext.Provider value={{ theme, toggleTheme }}>{children}</ThemeContext.Provider>;
};
