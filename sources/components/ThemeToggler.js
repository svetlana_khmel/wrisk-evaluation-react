import React, { useContext, memo } from 'react';
import { ThemeContext } from './ThemeProvider';

import '../scss/toggler.scss';

const ThemeToggler = () => {
  const { theme, toggleTheme } = useContext(ThemeContext);

  return (
    <button className="toggler" onClick={toggleTheme}>
      Switch to {theme === 'color' ? 'black' : 'color'} mode
    </button>
  );
};

export default memo(ThemeToggler);
