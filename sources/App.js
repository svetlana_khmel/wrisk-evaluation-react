import React, { useEffect, lazy, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getIp } from './actions';

import withSuspense from './components/HOC/withSuspense';
import Loader from './components/Loader';
import ThemeToggler from './components/ThemeToggler';
import { ThemeContext } from './components/ThemeProvider';

const City = lazy(() => import('./components/City'));
const Date = lazy(() => import('./components/Date'));
const Dashboard = lazy(() => import('./components/Dashboard'));
const Details = lazy(() => import('./components/Details'));

const CityWithSuspense = withSuspense(City);
const DashboarWithSuspense = withSuspense(Dashboard);
const DetailsWithSuspense = withSuspense(Details);
const DateWithSuspense = withSuspense(Date);

import './scss/app.scss';

const App = () => {
  const dispatch = useDispatch();
  const timelines = useSelector(state => state.timelines);
  const locationData = useSelector(state => state.locationData);

  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    dispatch(getIp());
  }, []);

  if (!timelines) return <Loader />;

  return (
    <div className={`container ${theme}`}>
      <ThemeToggler />
      <CityWithSuspense locationData={locationData} />
      <DateWithSuspense />
      <DashboarWithSuspense timelines={timelines} />
      <DetailsWithSuspense />
    </div>
  );
};

export default App;
