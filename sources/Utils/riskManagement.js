const TemperatureScale = num => {
  if (num < -60) {
    return 10;
  } else if (num < -50) {
    return 10;
  } else if (num < -40) {
    return 9;
  } else if (num < -30) {
    return 8;
  } else if (num < -20) {
    return 7;
  } else if (num < 10) {
    return 6;
  } else if (num > 0 && num < 5) {
    return 4;
  } else if (num > 5 && num < 10) {
    return 3;
  } else if (num > 10 && num < 20) {
    return 2;
  } else if (num > 20 && num < 30) {
    return 2;
  } else if (num > 30 && num < 40) {
    return 5;
  } else if (num > 40 && num < 50) {
    return 8;
  }
};

const PrecipitationProbability = num => {
  return num;
};

const Visibility = num => {
  if (num < 5) {
    return 10;
  } else if (num < 10) {
    return 8;
  } else if (num < 20) {
    return 7;
  } else if (num < 30) {
    return 6;
  } else if (num < 40) {
    return 5;
  } else if (num < 50) {
    return 4;
  } else if (num < 60) {
    return 3;
  } else if (num < 70) {
    return 1;
  } else if (num < 80) {
    return 0;
  }
};

const WindSpeed = num => {
  if (num > 100) {
    return 10;
  } else if (num > 80) {
    return 8;
  } else if (num > 70) {
    return 7;
  } else if (num > 60) {
    return 6;
  } else if (num > 50) {
    return 5;
  } else if (num > 40) {
    return 4;
  } else if (num > 30) {
    return 3;
  } else if (num < 20) {
    return 1;
  } else if (num < 10) {
    return 0;
  }
};

const riskEstimation = ruskNum => {
  if (ruskNum > 0 && ruskNum < 30) {
    return 'Low';
  } else if (ruskNum > 30 && ruskNum < 60) {
    return 'Medium';
  } else if (ruskNum > 80 && ruskNum < 80) {
    return 'High';
  } else if (ruskNum > 60 && ruskNum < 100) {
    return 'Super High';
  }
};

const riskCalculation = (temp, precipitation, visibility, windSpeed) =>
  TemperatureScale(temp) + PrecipitationProbability(precipitation) + Visibility(visibility) + WindSpeed(windSpeed);

export { riskEstimation, riskCalculation };
