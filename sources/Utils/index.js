const getTimezone = () => Intl.DateTimeFormat().resolvedOptions().timeZone;

const getDayTomorrow = () => {
  let date = new Date();
  return date.getDate() + 1;
};

const getCurrentMonth = () => {
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  const date = new Date();
  return months[date.getMonth()];
};

const getTime = future => {
  let date = new Date();
  date.setDate(date.getDate() + 1);

  if (future) {
    date = new Date(new Date().getTime() + 60 * 60 * (24 * 2) * 1000);
  }

  return date.toISOString();
};

const timeToHours = time => {
  const dateWithouthSecond = new Date(time);
  return dateWithouthSecond.toLocaleTimeString(navigator.language, { hour: '2-digit', minute: '2-digit' });
};

const mapCodeToImg = code => {
  switch (code) {
    case 4201:
      return 'rain_heavy';
    case 4001:
      return 'rain';
    case 4200:
      return 'rain_light';
    case 6201:
      return 'freezing_rain_heavy';
    case 6001:
      return 'freezing_rain';
    case 6200:
      return 'freezing_rain_lightfreezing_rain_light';
    case 6000:
      return 'freezing_drizzle';
    case 4000:
      return 'drizzle';
    case 7101:
      return 'ice_pellets_heavy';
    case 7000:
      return 'ice_pellets';
    case 7102:
      return 'ice_pellets_light';
    case 7102:
      return 'ice_pellets_light';
    case 5101:
      return 'snow_heavy';
    case 5000:
      return 'snow';
    case 5100:
      return 'snow_light';
    case 5001:
      return 'flurries';
    case 8000:
      return 'tstorm';
    case 2100:
      return 'fog_light';
    case 2000:
      return 'fog';
    case 1001:
      return 'cloudy';
    case 1102:
      return 'mostly_cloudy';
    case 1101:
      return 'partly_cloudy_day';
    case 1100:
      return 'mostly_clear_day';
    case 1000:
      return 'clear_day';

    default:
      return '';
  }
};

export { getTime, getTimezone, timeToHours, mapCodeToImg, getDayTomorrow, getCurrentMonth };
