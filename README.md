# Introduction

Screenshot:

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613748429/Screen_Shot_2021-02-19_at_5.22.35_PM_xhfey6.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613909303/Screen_Shot_2021-02-21_at_2.08.04_PM_cf14lr.png)

We would like you to integrate with a weather API and report the latest weather to the user.
A good API which can be used for free after registration can be found here:
https://www.climacell.co/

A good way of seeing example queries and what can be done with the API can be found here:
https://github.com/ClimaCell-API/climacell-postman

Once finished, please upload your source code (with README.md) to a private github repository and invite ​michaelkay-wrisk​ and ​aharin​ to contribute so we can have a look.

## Business Requirements

● When the user opens the application they should be presented with a timeline of what the weather's like in London, hourly, over the next day.

● When a user clicks on one of the items in the timeline then they should be presented with more detailed information on the weather at that time.

● Categorise the current weather (e.g. wind speed and visibility) into a risk score (percentage) so the user knows how risky it is to travel.

● How you present the data is up to you but ideally you would include icons for the current weather at any given time. Icons are given for free by climacell as long as you give credit

https://github.com/ClimaCell-API/weather-code-icons

## Bonus Points

● Base the weather off the user's current location using something like the browser geolocation API

● Use a CSS in JS framework for styling and allow us to change the theme of the app on the fly (everyone loves a dark mode!)

## Technical Requirements

● Use react to develop the application and treat it as a SPA

● Use the latest version of TypeScript.

● Use your favourite package manager (npm, yarn,...) to allow us to run your amazing
app locally.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
npm install
```

## Usage

```python
npm start
```

## Links

Get IP:
https://api.my-ip.io/ip

https://github.com/satansdeer/react-redux-typescript.
!!!https://www.newline.co/@satansdeer/using-react-redux-with-typescript--6ea90757

https://ipstack.com/signup/free

API Access Key 7fbae69ed27cf170a5be67b998f0d76d
http://api.ipstack.com/

http://api.ipstack.com/46.219.208.193?access_key=7fbae69ed27cf170a5be67b998f0d76d

http://api.ipstack.com/134.201.250.155
? access_key = YOUR_ACCESS_KEY

// optional parameters:

& fields = ip,location,security
& hostname = 1
& security = 1
& language = en
& callback = MY_CALLBACK
& output = json

https://docs.climacell.co/reference/post-timelines

/////https://app.climacell.co/ 3zTy9ndiLMZ9jzAjFqlPuXKBoPzcoF6B
